using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverMenu : MonoBehaviour
{
    public void ShowMenu() 
    {
        gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    public void CloseMenu()
    {
        gameObject.SetActive(false);

    }
}
