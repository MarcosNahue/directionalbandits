using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    [SerializeField] List<GameObject> lstEnemies = new List<GameObject>();
    [SerializeField] public Slider sliderGO;

    public float timerDuration = 60f; 
    public float currentTimer;

    public bool timerCanCount = true;

    private void Start()
    {
        currentTimer = timerDuration;
        sliderGO.maxValue = timerDuration;
        UpdateTimerUI();

        GameObject[] objectsWithTag = GameObject.FindGameObjectsWithTag("Enemy");
        lstEnemies.Clear();

        foreach (var item in objectsWithTag)
        {
            lstEnemies.Add(item);
        }         
    }
    private void Update()
    {
        if (currentTimer > 0 && timerCanCount)
        {
            currentTimer -= 1 * Time.deltaTime;
            UpdateTimerUI();
        }
        else
        {
            // Timer is done, implement your logic here
            Debug.Log("Timer finished!");
        }
    }

    public void RestartLevel() 
    {
        LeanTween.cancelAll();
        foreach (var item in lstEnemies)
        {
            item.GetComponent<Enemy>().resettingPosition = true;
            item.transform.position = item.GetComponent<Enemy>().startingPos;
        }
        
        foreach (var item in lstEnemies)
        {
            item.GetComponent<Enemy>().resettingPosition = false;
            item.gameObject.GetComponent<Enemy>().MoveEnemy();
        }

        Time.timeScale = 1;
    }
    private void UpdateTimerUI()
    {
        if (sliderGO != null)
        {
            sliderGO.value = currentTimer;
        }
    }
}
