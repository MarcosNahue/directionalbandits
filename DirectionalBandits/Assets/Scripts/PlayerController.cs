using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private GameObject sightPivotGO;
    [SerializeField] private GameObject cameraPivotGO;

    private GameObject playerGO;
    private GameObject gameControllerGO;

    private bool checkingPhone = false;
    private bool canMove = true;
    private bool executeOnce = true;

    private float currentRotation = -1;
    private float objectiveRotation = -1;

    void Start()
    {
        playerGO = this.gameObject;
        gameControllerGO = GameObject.FindWithTag("GameController");
    }


    void Update()
    {
        #region Movement
        CheckIfMoving();

        if (canMove && (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)))
        {
            canMove = false;
            objectiveRotation = 90;
            playerGO.transform.LeanRotateY(90, 0.3f);
        }

        if (canMove && (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)))
        {
            canMove = false;
            objectiveRotation = -90;
            playerGO.transform.LeanRotateY(-90, 0.3f);
        }

        if (canMove && (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)))
        {
            canMove = false;
            objectiveRotation = objectiveRotation + (currentRotation - 90);
            playerGO.transform.LeanRotateY(currentRotation - 90, 0.3f);
        }

        if (canMove && (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)))
        {
            canMove = false;
            objectiveRotation = objectiveRotation + (currentRotation + 90);
            playerGO.transform.LeanRotateY(currentRotation + 90, 0.3f);
        }
        #endregion

        if (Input.GetKeyDown(KeyCode.Space))
        {
            SwitchPhoneState();
        }

        if (Input.GetKeyDown(KeyCode.L) && checkingPhone == true)
        {
            // Pause the timer, subtract 2 from the timer if it's greater than 2 seconds remaining            
            if (gameControllerGO.GetComponent<GameController>().currentTimer > 2)
            {
                gameControllerGO.GetComponent<GameController>().timerCanCount = false;
                gameControllerGO.GetComponent<GameController>().sliderGO.value -= 0.5f;
                gameControllerGO.GetComponent<GameController>().currentTimer -= 0.5f;
                gameControllerGO.GetComponent<GameController>().timerCanCount = true;
            }
            
        }
    }

    private void CheckIfMoving() 
    {    
        do
        {
            objectiveRotation = currentRotation;
            executeOnce = false;
        } while (executeOnce);
        currentRotation = playerGO.transform.rotation.eulerAngles.y;
        if (Mathf.Approximately(currentRotation, objectiveRotation))
        {
            canMove = true;
        }
        else
        {
            canMove = false;
        }
    }

    private void SwitchPhoneState() 
    {
        
        if (checkingPhone == true) //Already checking
        {
            sightPivotGO.gameObject.transform.localScale = new Vector3(1, 1, 1);
            cameraPivotGO.gameObject.transform.LeanRotateX(0, 0.4f);

        }
        else //Check
        {
            sightPivotGO.gameObject.transform.localScale = new Vector3(1, 1, 0.45f);
            cameraPivotGO.gameObject.transform.LeanRotateX(35.5f, 0.4f);
        }
        checkingPhone = !checkingPhone;
    }

}
