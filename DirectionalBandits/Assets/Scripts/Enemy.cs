using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] GameObject playerGO;
    [SerializeField] private GameObject gameOverMenu;
    public Vector3 startingPos;

    private bool isMovingForward = false;
    public bool resettingPosition = false;

    private float forwardSpeed = 15f;
    private float backwardSpeed = 10f;

 

    void Start()
    {
        startingPos = gameObject.transform.position;
        MoveEnemy();
    }

    public void MoveEnemy() 
    {
        LeanTween.cancel(gameObject);

        if (resettingPosition == false)
        {
            if (isMovingForward == false) //FORWARD
            {
                LeanTween.move(gameObject, playerGO.transform.position, forwardSpeed);

            }
            else //BACKWARDS
            {
                LeanTween.move(gameObject, startingPos, backwardSpeed);
            }
        }
       
    
    }

    private void OnTriggerEnter(Collider other)
    {
        if (resettingPosition == false)
        {
            if (other.gameObject.CompareTag("SightRange"))
            {
                isMovingForward = !isMovingForward;
                MoveEnemy();
            }
            else if (other.gameObject.CompareTag("Player"))
            {
                gameOverMenu.GetComponent<GameOverMenu>().ShowMenu();

            }
        }
       
    }

    private void OnTriggerExit(Collider other)
    {
        if (resettingPosition == false)
        {
            if (other.CompareTag("SightRange"))
            {
                isMovingForward = !isMovingForward;
                MoveEnemy();
            }
        }    
    }
}
